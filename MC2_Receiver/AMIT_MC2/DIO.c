/*********************************************************** DIO.c File ********************************************************************/
/*************************************************** Including Requiered .h File ***********************************************************/
#include "DIO.h"																									/* ATMega-32 Drivers*/
/***************************************************** Startig The Funcction ***************************************************************/
void DIO_VSetPinDir(char PORT_LETTER,char PIN_NUMBER,char STATE)																		/* PORT-letter: ('A','B','C' Or 'D') / PIN_NUmber: ( 0,1,2,3,4,5,6,7 ) / STATE: ( h/H: On , l/L: Off ) */
{																																		/* */
	switch(STATE)																														/* Switch Statement To Check The Vale Of 'STATE' (ON Or OFF) (OUTPUT Or INPUT) */
	{																																	/* */
		case	'h':																													/* */
		case	'H':																													/* '1' For HIGH (ON) */
		switch(PORT_LETTER)																												/* Switch Statement That Checks For The Value Of 'PORT_LETTER' To Open It*/
		{																																/* */
			case	'a':																												/* */
			case	'A':																												/* */
			DDRA |= (HIGH<<PIN_NUMBER);																									/* Will Set The Desired Port Known From 'PIN_NUMBER' To HIGH (ON)(OUTPUT)*/
			break;																														/* */
			case	'b':																												/* */
			case	'B':																												/* */
			DDRB |= (HIGH<<PIN_NUMBER);																									/* */
			break;																														/* */
			case	'c':																												/* */
			case	'C':																												/* */
			DDRC |= (HIGH<<PIN_NUMBER);																									/* */
			break;																														/* */
			case	'd':																												/* */
			case	'D':																												/* */
			DDRD |= (HIGH<<PIN_NUMBER);																									/* */
			break;																														/* */
			default:																													/* */
			break;																														/* */
		}																																/* */
		break;																															/* */
		case	'l':																													/* */
		case	'L':																													/* If '0' Then It Will Make The PIN (OFF)(LOW)(INPUT)*/
		switch(PORT_LETTER)																												/* */
		{																																/* */
			case	'a':																												/* */
			case	'A':																												/* */
			DDRA &= ~(HIGH<<PIN_NUMBER);																								/* Turns The Desired Pin To (OFF)(LOW)(INPUT)*/
			break;																														/* */
			case	'b':																												/* */
			case	'B':																												/* */
			DDRB &= ~(HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			case	'c':																												/* */
			case	'C':																												/* */
			DDRC &= ~(HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			case	'd':																												/* */
			case	'D':																												/* */
			DDRD &= ~(HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			default:																													/* */
			break;																														/* */
		}																																/* */
		break;																															/* */
		default:																														/* */
		break;																															/* */
	}																																	/* */
}																																		/* */
/* */
void DIO_VSetAllPinDir(char PORT_LETTER,char STATE)																						/* */
{																																		/* */
	switch(STATE)																														/* Switch Statement To Check The Vale Of 'STATE' (ON Or OFF) (OUTPUT Or INPUT) */
	{																																	/* */
		case	'h':																													/* */
		case	'H':																													/* '1' For HIGH (ON) */
		switch(PORT_LETTER)																												/* Switch Statement That Checks For The Value Of 'PORT_LETTER' To Open It*/
		{																																/* */
			case	'a':																												/* */
			case	'A':																												/* */
			DDRA  = (HIGH<<PORT0);																										/* Set DDRA PORT0 To On*/
			DDRA |= (HIGH<<PORT1);																										/* Set DDRA PORT1 To On*/
			DDRA |= (HIGH<<PORT2);																										/* Set DDRA PORT2 To On*/
			DDRA |= (HIGH<<PORT3);																										/* Set DDRA PORT3 To On*/
			DDRA |= (HIGH<<PORT4);																										/* Set DDRA PORT4 To On*/
			DDRA |= (HIGH<<PORT5);																										/* Set DDRA PORT5 To On*/
			DDRA |= (HIGH<<PORT6);																										/* Set DDRA PORT6 To On*/
			DDRA |= (HIGH<<PORT7);																										/* Set DDRA PORT7 To On*/
			break;																														/* */
			case	'b':																												/* */
			case	'B':																												/* */
			DDRB  = (HIGH<<PORT0);																										/* Set DDRB PORT0 To On*/
			DDRB |= (HIGH<<PORT1);																										/* Set DDRB PORT1 To On*/
			DDRB |= (HIGH<<PORT2);																										/* Set DDRB PORT2 To On*/
			DDRB |= (HIGH<<PORT3);																										/* Set DDRB PORT3 To On*/
			DDRB |= (HIGH<<PORT4);																										/* Set DDRB PORT4 To On*/
			DDRB |= (HIGH<<PORT5);																										/* Set DDRB PORT5 To On*/
			DDRB |= (HIGH<<PORT6);																										/* Set DDRB PORT6 To On*/
			DDRB |= (HIGH<<PORT7);																										/* Set DDRB PORT7 To On*/
			break;																														/* */
			case	'c':																												/* */
			case	'C':																												/* */
			DDRC  = (HIGH<<PORT0);																										/* Set DDRC PORT0 To On*/
			DDRC |= (HIGH<<PORT1);																										/* Set DDRC PORT1 To On*/
			DDRC |= (HIGH<<PORT2);																										/* Set DDRC PORT2 To On*/
			DDRC |= (HIGH<<PORT3);																										/* Set DDRC PORT3 To On*/
			DDRC |= (HIGH<<PORT4);																										/* Set DDRC PORT4 To On*/
			DDRC |= (HIGH<<PORT5);																										/* Set DDRC PORT5 To On*/
			DDRC |= (HIGH<<PORT6);																										/* Set DDRC PORT6 To On*/
			DDRC |= (HIGH<<PORT7);																										/* Set DDRC PORT7 To On*/
			break;																														/* */
			case	'd':																												/* */
			case	'D':																												/* */
			DDRD  = (HIGH<<PORT0);																										/* Set DDRD PORT0 To On*/
			DDRD |= (HIGH<<PORT1);																										/* Set DDRD PORT1 To On*/
			DDRD |= (HIGH<<PORT2);																										/* Set DDRD PORT2 To On*/
			DDRD |= (HIGH<<PORT3);																										/* Set DDRD PORT3 To On*/
			DDRD |= (HIGH<<PORT4);																										/* Set DDRD PORT4 To On*/
			DDRD |= (HIGH<<PORT5);																										/* Set DDRD PORT5 To On*/
			DDRD |= (HIGH<<PORT6);																										/* Set DDRD PORT6 To On*/
			DDRD |= (HIGH<<PORT7);																										/* Set DDRD PORT7 To On*/
			break;																														/* */
			default:																													/* */
			break;																														/* */
		}																																/* */
		break;																															/* */
		case	'l':																													/* */
		case	'L':																													/* If '0' Then It Will Make The PIN (OFF)(LOW)(INPUT)*/
		switch(PORT_LETTER)																												/* */
		{																																/* */
			case	'a':																												/* */
			case	'A':																												/* */
			DDRA &= ~(HIGH<<PORT0);																										/* Set DDRA PORT0 To Off*/
			DDRA &= ~(HIGH<<PORT1);																										/* Set DDRA PORT1 To Off*/
			DDRA &= ~(HIGH<<PORT2);																										/* Set DDRA PORT2 To Off*/
			DDRA &= ~(HIGH<<PORT3);																										/* Set DDRA PORT3 To Off*/
			DDRA &= ~(HIGH<<PORT4);																										/* Set DDRA PORT4 To Off*/
			DDRA &= ~(HIGH<<PORT5);																										/* Set DDRA PORT5 To Off*/
			DDRA &= ~(HIGH<<PORT6);																										/* Set DDRA PORT6 To Off*/
			DDRA &= ~(HIGH<<PORT7);																										/* Set DDRA PORT7 To Off*/
			break;																														/* */
			case	'b':																												/* */
			case	'B':																												/* */
			DDRB &= ~(HIGH<<PORT0);																										/* Set DDRB PORT0 To Off*/
			DDRB &= ~(HIGH<<PORT1);																										/* Set DDRB PORT1 To Off*/
			DDRB &= ~(HIGH<<PORT2);																										/* Set DDRB PORT2 To Off*/
			DDRB &= ~(HIGH<<PORT3);																										/* Set DDRB PORT3 To Off*/
			DDRB &= ~(HIGH<<PORT4);																										/* Set DDRB PORT4 To Off*/
			DDRB &= ~(HIGH<<PORT5);																										/* Set DDRB PORT5 To Off*/
			DDRB &= ~(HIGH<<PORT6);																										/* Set DDRB PORT6 To Off*/
			DDRB &= ~(HIGH<<PORT7);																										/* Set DDRB PORT7 To Off*/
			break;																														/* */
			case	'c':																												/* */
			case	'C':																												/* */
			DDRC &= ~(HIGH<<PORT0);																										/* Set DDRC PORT0 To Off*/
			DDRC &= ~(HIGH<<PORT1);																										/* Set DDRC PORT1 To Off*/
			DDRC &= ~(HIGH<<PORT2);																										/* Set DDRC PORT2 To Off*/
			DDRC &= ~(HIGH<<PORT3);																										/* Set DDRC PORT3 To Off*/
			DDRC &= ~(HIGH<<PORT4);																										/* Set DDRC PORT4 To Off*/
			DDRC &= ~(HIGH<<PORT5);																										/* Set DDRC PORT5 To Off*/
			DDRC &= ~(HIGH<<PORT6);																										/* Set DDRC PORT6 To Off*/
			DDRC &= ~(HIGH<<PORT7);																										/* Set DDRC PORT7 To Off*/
			break;																														/* */
			case	'd':																												/* */
			case	'D':																												/* */
			DDRD &= ~(HIGH<<PORT0);																										/* Set DDRD PORT0 To Off*/
			DDRD &= ~(HIGH<<PORT1);																										/* Set DDRD PORT1 To Off*/
			DDRD &= ~(HIGH<<PORT2);																										/* Set DDRD PORT2 To Off*/
			DDRD &= ~(HIGH<<PORT3);																										/* Set DDRD PORT3 To Off*/
			DDRD &= ~(HIGH<<PORT4);																										/* Set DDRD PORT4 To Off*/
			DDRD &= ~(HIGH<<PORT5);																										/* Set DDRD PORT5 To Off*/
			DDRD &= ~(HIGH<<PORT6);																										/* Set DDRD PORT6 To Off*/
			DDRD &= ~(HIGH<<PORT7);																										/* Set DDRD PORT7 To Off*/
			break;																														/* */
			default:																													/* */
			break;																														/* */
		}																																/* */
		break;																															/* */
		default:																														/* */
		break;																															/* */
	}																																	/* */
}																																		/* */
/* */
void DIO_VSetPort_IO(char PORT_LETTER,char PIN_NUMBER,char STATE)																		/* PORT-letter: ('A','B','C' Or 'D') / PIN_NUmber: ( 0,1,2,3,4,5,6,7 ) / STATE: ( h/H: On , l/L: Off ) */
{																																		/* */
	switch(STATE)																														/* Switch Statement To Check The Vale Of 'STATE' (ON Or OFF) (OUTPUT Or INPUT) */
	{																																	/* */
		case	'h':																													/* */
		case	'H':																													/* '1' For HIGH (ON) */
		switch(PORT_LETTER)																												/* Switch Statement That Checks For The Value Of 'PORT_LETTER' To Open It*/
		{																																/* */
			case	'a':																												/* */
			case	'A':																												/* */
			PORTA |= (HIGH<<PIN_NUMBER);																								/* Will Put Value 1 In The Port (ON)*/
			break;																														/* */
			case	'b':																												/* */
			case	'B':																												/* */
			PORTB |= (HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			case	'c':																												/* */
			case	'C':																												/* */
			PORTC |= (HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			case	'd':																												/* */
			case	'D':																												/* */
			PORTD |= (HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			default:																													/* */
			break;																														/* */
		}																																/* */
		break;																															/* */
		case	'l':																													/* */
		case	'L':																													/* If '0' Then It Will Make The PIN (OFF)(LOW)(INPUT)*/
		switch(PORT_LETTER)																												/* */
		{																																/* */
			case	'a':																												/* */
			case	'A':																												/* */
			PORTA &= ~(HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			case	'b':																												/* */
			case	'B':																												/* */
			PORTB &= ~(HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			case	'c':																												/* */
			case	'C':																												/* */
			PORTC &= ~(HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			case	'd':																												/* */
			case	'D':																												/* */
			PORTD &= ~(HIGH<<PIN_NUMBER);																								/* */
			break;																														/* */
			default:																													/* */
			break;																														/* */
		}																																/* */
		break;																															/* */
	}																																/* */
}																																		/* */
/* */
void DIO_VSetAllPort_IO(char PORT_LETTER,char STATE)																					/* To Set The Value Of The Whole Port.  PORT-letter: ('A','B','C' Or 'D') / STATE: ( h/H: 1 , l/L: 0 ) */
{																																		/* */
	switch(STATE)																														/* Switch Statement To Check The Vale Of 'STATE' (ON Or OFF) (OUTPUT Or INPUT) */
	{																																	/* */
		case	'h':																													/* */
		case	'H':																													/* '1' For HIGH (ON) */
		switch(PORT_LETTER)																												/* Switch Statement That Checks For The Value Of 'PORT_LETTER' To Open It*/
		{																																/* */
			case	'a':																												/* */
			case	'A':																												/* */
			PORTA |= WHOLE_PORT_UP;																										/* Will Put Value 1 In The Port (ON)*/
			break;																														/* */
			case	'b':																												/* */
			case	'B':																												/* */
			PORTB |= WHOLE_PORT_UP;																										/* */
			break;																														/* */
			case	'c':																												/* */
			case	'C':																												/* */
			PORTC |= WHOLE_PORT_UP;																										/* */
			break;																														/* */
			case	'd':																												/* */
			case	'D':																												/* */
			PORTD |= WHOLE_PORT_UP;																										/* */
			break;																														/* */
			default:																													/* */
			break;																														/* */
		}																																/* */
		break;																															/* */
		case	'l':																													/* */
		case	'L':																													/* If '0' Then It Will Make The PIN (OFF)(LOW)(INPUT)*/
		switch(PORT_LETTER)																												/* */
		{																																/* */
			case	'a':																												/* */
			case	'A':																												/* */
			PORTA = WHOLE_PORT_DOWN;																									/* */
			break;																														/* */
			case	'b':																												/* */
			case	'B':																												/* */
			PORTB = WHOLE_PORT_DOWN;																									/* */
			break;																														/* */
			case	'c':																												/* */
			case	'C':																												/* */
			PORTC = WHOLE_PORT_DOWN;																									/* */
			break;																														/* */
			case	'd':																												/* */
			case	'D':																												/* */
			PORTD = WHOLE_PORT_DOWN;																									/* */
			break;																														/* */
			default:																													/* */
			break;																														/* */
		}																																/* */
		break;																															/* */
	}																																	/* */
}																																		/* */
/******************************************************** Read Funcion *********************************************************************/
char DIO_CReadPin(char PORT_LETTER,char PIN_NUMBER)																						/* A Functiom To Check A Certain Port If it's on or off by knowing Its Letter and Number. EX: DIO_CReadPIN('D',6);*/
{																																		/* */
	char Read='0';																														/* Initializing */
	switch(PORT_LETTER)																													/* */
	{																																	/* */
		case	'a':																													/* */
		case	'A':																													/* */
		Read =(PINA & (HIGH<<PIN_NUMBER));																								/* If The Port Is already on the value will be 1, If off the value will be 0 */
		break;																															/* */
		case	'b':																													/* */
		case	'B':																													/* */
		Read= (PINB & (HIGH<<PIN_NUMBER));																								/* */
		break;																															/* */
		case	'c':																													/* */
		case	'C':																													/* */
		Read= (PINC & (HIGH<<PIN_NUMBER));																								/* */
		break;																															/* */
		case	'd':																													/* */
		case	'D':																													/* */
		Read= (PIND & (HIGH<<PIN_NUMBER));																								/* */
		break;																															/* */
		default:																														/* */
		break;																															/* */
	}																																	/* */
	Read = (Read>>PIN_NUMBER);																											/* */
	return Read;																														/* */
}																																		/* */
/*********************************************************** End Of Text *******************************************************************/