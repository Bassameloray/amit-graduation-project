/********************************************** The Second Micro-Controller Body ****************************************************/
/* AMIT_MC2 Is The Project Responsible For Printing On The LCD As Well As Working As A Receiver To AMIT_MC1 Using The SPI Protocol  */
/***********************************************************************************************************************************/
/******************************************** Including The Required Library Files *************************************************/
#include <avr/io.h>																												/* Including The Main AVR Library */
#define F_CPU 8000000UL																											/* Defining The CPU Clock */
#include <util/delay.h>																											/* Including The Delay Library */
#include <avr/interrupt.h>																										/* Including The Interrupt Library */
#include "DIO.h"																												/* Including The MCAL Drivers Library */
#include "LCD_Driver.h"																											/* Including The LCD Driver */
#include "KeyPadDriver.h"																										/* */
#include "SPI.h"																												/* Including The SPI Communication Protocol Driver */
/************************************************** Starting The Main Body *********************************************************/
int main(void)																													/* The Main Function */
{																																/* */
																																/* */
	char Temp = 0, Inputs_Counter =0;																							/* Initializing Temporary Char Variable To Store The Data At And Counter For The Inputs*/
	char Inputs[8] = {0,0,0,0,0,0,0,0};																							/* Initializing Array Of Short Variables To Store The Inputs*/
	char Result[4] = {0,0,0,0};																									/* Initializing An Int Variable To Store The Result*/
																																/* */
	LCD_vInit_4BitMode();																										/* Initializing The Keypad */
	SPI_SlaveInit();																											/* Initializing The MC As A Transmitter */
	DDRC = 0xff;																												/* Opening The Port For EEPROM Result */
	KeyPad_vInit();																												/* Initializing The Keypad*/
	LCD_vSendCmd_4BitMode(MODE_4_BITS);																							/* Work As 4 Bit Mode*/
	LCD_vSendCmd_4BitMode(CLEAR_LCD);																							/* Clear The LCD*/
	LCD_vSendCmd_4BitMode(CURSOR_ON);																							/* Turn The Cursor On*/
																																/* */
	while(1)																													/* The Main APP's While Loop */
	{	
		Temp = 0;
		Inputs_Counter =0;
		Inputs[0]=0, Inputs[1]=0, Inputs[2]=0,Inputs[3]=0,Inputs[4]=0,Inputs[5]=0,Inputs[6]=0,Inputs[7]=0,Inputs[8]=0;
		Result[0]=0,Result[1]=0,Result[2]=0,Result[3]=0,Result[4]=0;															/* */
		while(Temp!= '+')																										/* */
		{																														/* */
			Temp = (char)SPI_SlaveReceive();																					/* Reading The Keypad And Storing The Result In Char Variable 'Temp'*/
			if((Temp != NO_INPUT)&&(Inputs_Counter<=3))																			/* If The Input Is Number*/
			{																													/* */
				LCD_vSendData_4BitMode(Temp);																					/* Print The Temp On The LCD*/
				/*_delay_ms(300);*/																								/* */
				Inputs[Inputs_Counter] = Temp;																					/* */
				Inputs_Counter++;																								/* */
			}																													/* */
			else if(Inputs_Counter>3)																							/* Else*/
			{																													/* */
				LCD_vPrintString_4BitMode("Wrong Input.Start Again");															/* */
				Inputs[6]=0;																									/* */
				Inputs_Counter=0;																								/* */
				Temp= NO_INPUT;																									/* */
			}																													/* */
			else																												/* */
			{																													/* */
																																/* */
			}																													/* */
																																/* */
		}																														/* */
																																/* */
		/*_delay_ms(300);*/																											/* Delay 0.3 Second Not To Give The User A Chance To Move His Finger Away From The Botton*/
		Inputs_Counter =4;																										/* */
		Temp = NO_INPUT;																										/* */
		while(Temp!= '=')																										/* */
		{																														/* */
			Temp = (char)SPI_SlaveReceive();																					/* Reading The Keypad And Storing The Result In Char Variable 'Temp'*/
			if((Temp != NO_INPUT)&&(Inputs_Counter<=6))																			/* If The Input Is Number*/
			{																													/* */
				LCD_vSendData_4BitMode(Temp);																					/* Print The Temp On The LCD*/
				/*_delay_ms(300);	*/																								/* */
				Inputs[Inputs_Counter] = (short)Temp;																			/* */
				Inputs_Counter++;																								/* */
			}																													/* */
			else if(Inputs_Counter>7)																							/* Else*/
			{																													/* */
				LCD_vPrintString_4BitMode("Wrong Input.Start Again");															/* */
				Inputs[4]=0,Inputs[5]=0,Inputs[6]=0;																			/* */
				Inputs_Counter=4;																								/* */
				Temp= NO_INPUT;																									/* */
			}																													/* */
			else																												/* */
			{																													/* */
																																/* */
			}																													/* */
																																/* */
		}																														/* */
		_delay_ms(300);																											/* */
		LCD_vSendCmd_4BitMode(CLEAR_LCD);																						/* Clear The LCD*/
		LCD_vPrintString_4BitMode("The Result Is:");																			/* */
		_delay_ms(300);																											/* */
		LCD_vSendCmd_4BitMode(CLEAR_LCD);																						/* Clear The LCD*/
																																/* */
		Result[2]+= (Inputs[2]-'0')+(Inputs[6]);																				/* */
		while(Result[2]>=58)																									/* */
		{																														/* */
			Result[2]-=10;																										/* */
			Result[1]+=1;																										/* */
		}																														/* */
		Result[1]+= (Inputs[1]-'0')+(Inputs[5]);																				/* */
		while(Result[1]>=58)																									/* */
		{																														/* */
			Result[1]-=10;																										/* */
			Result[0]+=1;																										/* */
		}																														/* */
		Result[0]+= (Inputs[0]-'0')+(Inputs[4]);																				/* */
			if(Result[0]>=58)																									/* */
			{																													/* */
			LCD_vSendCmd_4BitMode(CLEAR_LCD);																					/* Clear The LCD*/
			LCD_vPrintString_4BitMode("Over-Flow");																				/* */
			_delay_ms(10000);																									/* */
			LCD_vSendCmd_4BitMode(CLEAR_LCD);																					/* Clear The LCD*/
		}																														/* */
			else if(Result[0]<58)																								/* */
			{																													/* */
			LCD_vSendCmd_4BitMode(CLEAR_LCD);																					/* Clear The LCD*/
			_delay_ms(300);																										/* */
			LCD_vSendData_4BitMode(Result[0]);																					/* */
			LCD_vSendData_4BitMode(Result[1]);																					/* */
			LCD_vSendData_4BitMode(Result[2]);																					/* */
			PORTC = Result[0]+Result[1]+Result[2];																				/* */																							/* */
			_delay_ms(2000);																									/* */
			LCD_vSendCmd_4BitMode(CLEAR_LCD);																					/* Clear The LCD*/
			}																													/* */
	}																															/* */
}																																/* */
/******************************************************** End Of Text **************************************************************/