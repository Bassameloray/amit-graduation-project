#include <avr/io.h>																												/* Including The Main AVR Library */
#define F_CPU 8000000UL																											/* Defining The CPU Clock */
#include <util/delay.h>																											/* Including The Delay Library */
#include <avr/interrupt.h>																										/* Including The Interrupt Library */
#include "DIO.h"																												/* Including The MCAL Drivers Library */
#include "LCD_Driver.h"																											/* Including The LCD Driver */
#include "SPI.h"

void SPI_MasterInit(void)
{
	/*Set clock to fosc/16*/
	SPCR = (1<<SPR0);
	/*SPCR |= (0<<SPR1);*/
	/*SPCR |= (0<<SPI2X);*/

	/*Enable Master mode*/
	SPCR |= (1<<MSTR);
	/*At the End,  Enable SPI*/
	SPCR |= (1<<SPE);
	/*Set SS--> Pin 4 / MOSI --> Pin 5 / SCK --> Pin 7 as outputs for master*/
	DDRB |= (1<<4); /* SS Pin*/
	DDRB |= (1<<5); /* MOSI Pin*/
	DDRB |= (1<<7); /* SCK Pin*/
	 
	/*Set SS Pin to high*/
	PORTB |= (1<<4);
}

void SPI_MasterTransmit(char Data)
{
	/*Clear SS to write to slave*/
	PORTB &= (~(1<<4)); /* SS Port To Low*/
	/*Put data on bus*/
	SPDR = Data;
	/*Wait until the transmission is finished*/
	while((SPSR&(1<<SPIF)) == 0)
	{
		
	}
	/*Set SS to high*/
	PORTB = (1<<4);
}

void SPI_SlaveInit(void)
{
	/*Enable SPI*/
	SPCR |= (1<<SPE);
	/*Set MISO as output*/
	DDRB |= (1<<6);
}

char SPI_SlaveReceive(void)
{
	
	/*Wait until data is received in SPI register*/
		while((SPSR&(1<<SPIF)) == 0)
		{
			
		}
	/*The data arrived read it*/
	return SPDR;
}