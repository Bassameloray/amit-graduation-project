/******************************************** Starting The '.h' File ***************************************************/
#ifndef DIO_H_																										/* If The Headers Are not defined yet it will be defined, if it was already defined it won't be defined again */
#define DIO_H_																										/* */

#include <avr/io.h>																									/* Main Input/Output Header*/
#define F_CPU 8000000UL																								/* Delay Cycle*/
#include <util/delay.h>																								/* Delay Header*/
#include <avr/interrupt.h>																							/* The Interrupt Header File*/

/*********************************************** Macros For DIO.C ******************************************************/
#define LED2	7																										/* LED2 Shift*/
#define LED1	6																										/* LED1 Shift*/
#define LED0	5																										/* LED0 Shift*/
#define BUZZER  4																										/* Buzzer Shift*/
#define RELAY   3																										/* Relay Shift*/
#define BUTTON2	2																										/* Button2 Shift*/
#define BUTTON1	1																										/* Button1 Shift*/
#define BUTTON0	0																										/* Button0 Shift*/
#define OUTPUT	1																										/* Set The Pin To Output*/
#define INPUT	0																										/* Set The Pin To Input*/
#define HIGH	1																										/* ON */
#define LOW		0																										/* OFF */
#define PORT0	0																										/* */
#define PORT1	1																										/* */
#define PORT2	2																										/* */
#define PORT3	3																										/* */
#define PORT4	4																										/* */
#define PORT5	5																										/* */
#define PORT6	6																										/* */
#define PORT7	7																										/* */
#define BIT1	1																										/* */
#define BIT2	2																										/* */
#define BIT3	3																										/* */
#define BIT4	4																										/* */
#define BIT5	5																										/* */
#define BIT6	6																										/* */
#define BIT7	7																										/* */
#define BIT8	8																										/* */
#define WHOLE_PORT_UP 0xff																								/* */
#define WHOLE_PORT_DOWN 0x00																							/* */
/************************************************ End Of Macros ********************************************************/
/*********************************************** Macro Functions *******************************************************/
#define SET_BIT(REGISTER,BIT_NUM) (REGISTER = REGISTER | (1<<BIT_NUM))													/* Sets A Certain Port To Output (1) */
#define CLEAR_BIT(REGISTER,BIT_NUM) (REGISTER = REGISTER & ~(1<<BIT_NUM))												/* Resets The Port To Input (0) */
#define CLEAR_ALL_BITS(REGISTER) (REGISTER = REGISTER & 0)																/* Resets All Bits To Inputs */
#define TOGGLE_BIT(REGISTER,BIT_NUM) (REGISTER ^= REGISTER | (1<<BIT_NUM))												/* Toggles A Bit. If The Bit is Output It Will Become Input and vice versa */
#define IS_BIT_SET(REGISTER,BIT_NUM) ((REGISTER & (1<<BIT_NUM))>>BIT_NUM)												/* Reads A Certain Bit State, If Output It Will Return '1', If Input It Will Return '0'. PS: Example: "State = Is_Bit_Set(PORTD,LED2);" Then Check The Value Of 'State' */
#define IS_BIT_CLEAR(REGISTER,BIT_NUM) ((!(REGISTER & (1<<BIT_NUM)))>>BIT_NUM)											/* Reads A Certain Bit State, If Output It Will Return '0', If Input It Will Return '1'. */
#define ROR(REF,BIT_NUM) (REG = (REG>>BIT_NUM) | (REG<<(8-BIT_NUM)))													/* Rotates The Whole Byte's Bits To The Right By A Certain Rotation Steps */
#define ROL(REF,BIT_NUM) (REG = (REG<<BIT_NUM) | (REG>>(8-BIT_NUM)))													/* Rotates The Whole Byte's Bits To The Left By A Certain Rotation Steps */
/******************************************* End Of Macro Functions ****************************************************/
/************************************************ Prototyping **********************************************************/
void DIO_VSetPinDir(char PORT_LETTER,char PIN_NUMBER,char STATE);													/* To Set The Port To Input Or Outpur. Ex: DIO-VSetPinDir( (A,B,C Or D) , ( 0,1,2,3,4,5,6,7 ) , (ON Or OFF) ) */
void DIO_VSetAllPinDir(char PORT_LETTER,char STATE);																/* To Set All DDR-Port On Or Off */
void DIO_VSetPort_IO(char PORT_LETTER,char PIN_NUMBER,char STATE);													/* To Set The Value Of The Port*/
void DIO_VSetAllPort_IO(char PORT_LETTER,char STATE);																/* To Set The Value Of The Whole Port*/
char DIO_CReadPin(char PORT_LETTER,char PIN_NUMBER);																/* To Read The State Of A Certain Port */
/************************************************ End Of Flie **********************************************************/
#endif /****************************************************************************************************************/