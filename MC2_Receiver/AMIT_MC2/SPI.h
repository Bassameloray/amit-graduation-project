/*
 * SPI.h
 *
 * Created: 9/5/2017 8:34:54 PM
 *  Author: Cennan
 */ 


#ifndef SPI_H_
#define SPI_H_

#include <avr/io.h>																												/* Main Input/Output Header*/
#define F_CPU 8000000UL																											/* Delay Cycle*/
#include <util/delay.h>																											/* Delay Header*/
#include <avr/interrupt.h>																										/* The Interrupt Header File*/

void SPI_MasterInit(void);
void SPI_MasterTransmit(char Data);

void SPI_SlaveInit(void);
char SPI_SlaveReceive(void);

#endif /* SPI_H_ */