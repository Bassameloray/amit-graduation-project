/********************************************** The First Micro Controller Body ****************************************************/
/* AMIT_MC1 Is The Project Responsible For Reading The Keypad And ADC As Well As Working As A Transmitter To AMIT_MC2 Using		****/
/* The SPI Communication Protocol																								****/
/***********************************************************************************************************************************/
/******************************************** Including The Required Library Files *************************************************/
#include <avr/io.h>																												/* Including The Main AVR Library */
#define F_CPU 8000000UL																											/* Defining The CPU Clock */
#include <avr/interrupt.h>																										/* Including The Interrupt Library */
#include <util/delay.h>																											/* Including The Delay Library */
#include "DIO.h"																												/* Including The MCAL Drivers Library */
#include "LCD_Driver.h"																											/* Including The LCD Driver Because Some Used MACROS At The Keypad Driver Are Here */
#include "KeyPadDriver.h"																										/* Including The Keypad Driver */
#include "SPI.h"																												/* Including The SPI Communication Protocol Driver */
#include "ADC_Driver.h"																											/* ADC Driver */
/************************************************** Starting The Main Body *********************************************************/

int main(void)																													/* The Main Function */
{																																/* */
	volatile char Temp=0;																										/* */
	KeyPad_vInit();																												/* Initializing The Keypad */
	SPI_MasterInit();																											/* Initializing The MC As A Transmitter */
	ADC_vInit();																												/* Initializing The ADC*/
	DIO_VSetAllPinDir('C','h');																									/* Setting The Registers For The ADC*/
	DIO_VSetAllPinDir('A','l');																									/* Setting The Registers For The ADC*/
	DIO_VSetPinDir('A',7,'h');																									/* Setting The Registers For The ADC*/
	DIO_VSetPinDir('A',6,'h');																									/* Setting The Registers For The ADC*/
																																/* */
	while(1)																													/* The Main APP's While Loop */
    {																															/* */
		ADC_vStartConversion();																									/* Reading The ADC Value And Reacting Depending On It*/
		if(((Keypad_u8press())==(NO_INPUT)))																					/* If The User Didn't Press The KeyPad */
		{																														/* */
			;																													/* Do Nothing */
		}																														/* */
		else if(((Keypad_u8press())!=(NO_INPUT)))																				/* If The User Pressed The KeyPad */
		{																														/* */
			Temp = (char)Keypad_u8press();																						/* */
			SPI_MasterTransmit(Temp);																							/* Send The Pressed Key To The Receiver Using SPI Communication Protocol */
			_delay_ms(250);																										/* DELAY */
		}																														/* */
		else																													/* If You Had Unexpected Behavior */
		{																														/* */
																																/* Do Nothing */
		}																														/* */
    }																															/* The End Of The Main While Loop */
}																																/* The End Of The Main Function */																												/* */
/******************************************************** End Of Text **************************************************************/