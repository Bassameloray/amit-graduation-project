#ifndef LCD_H_																											/* */
#define LCD_H_																											/* */
/******************************************* Opening Projects On And Off ***************************************************/
/* #define BITS_MODE_8 1 */																								/* To Turns 8 Bits Mode Functions Only Un-Comment This Line And Comment The Next Line */
/****************************** To Use Both 8&4 Bits Mode, Un-Comment The Previous Two Lines *******************************/
#include "DIO.h"																										/* Compile The 4 Bit Mode Functions Only*/
/************************************************ Magic Numbers' Macro *****************************************************/
#define MODE_8_BITS 0x38																								/* */
#define CLEAR_LCD 0x01																									/* */
#define CURSOR_ON 0x0e																									/* */
#define FIRST_ROW 1																										/* */
#define SECOND_ROW 2																									/* */
#define Zero_ROW 0																										/* */
#define FIFTH_ROW 4																										/* */
#define MODE_4_BITS 0x28																								/* */
#define RETURN_CURSOR_TO_HOME 0x02																						/* */
#define FIRST_ROW_CURSOR_HOME 0x80																						/* */
#define SECOND_ROW_CURSOR_HOME 0xC0																						/* */
#define COLUMN0 0																										/* */
#define COLUMN1 0																										/* */
#define COLUMN2 1																										/* */
#define COLUMN3 2																										/* */
#define COLUMN4 3																										/* */
#define COLUMN8 8																										/* */
/******************************************** End Of Magic Numbers' Macro **************************************************/
/******************************************** Some LCD Commands add When Needed ********************************************/
/***** Make Sure To Add 0x Before the Command And To Name The Macro A Name That Indicates The Function Of The Command ******/
/*Command*/																												/*Function*/
/*0x0F		*/																											/*LCD ON, Cursor ON, Cursor blinking ON*/
/*0x01			*/																										/*Clear screen*/
/*0x02				*/																									/*Return home*/
/*0X04					*/																								/*Decrement cursor*/
/*0X06						*/																							/*Increment cursor*/
/*0X0E							*/																						/*Display ON ,Cursor blinking OFF*/
/*0x80								*/																					/*Force cursor to the beginning of  1st line*/
/*0xC0									*/																				/*Force cursor to the beginning of 2nd line*/
/*0X38										*/																			/*Use 2 lines and 5�7 matrix*/
/*0X83											*/																		/*Cursor line 1 position 3*/
/*0X3C												*/																	/*Activate second line*/
/*0X08													*/																/*Display OFF, Cursor OFF*/
/*0XC1														*/															/*Jump to second line, position1*/
/*0XOC															*/														/*Display ON, Cursor OFF*/
/*0XC1																*/													/*Jump to second line, position1*/
/*0xC2																	*/												/*Jump to second line, position2*/
/************************************************ Functions Prototypes *****************************************************/
/************************************** General Functions That Work For Both Modes *****************************************/
void SendFallingEdge(void);																								/* */
/****************************************************** 8 Bits *************************************************************/
																														/* If The 8 Bits Mode Is Enabled The Following Functions Will Be Prototyped (Check The Macros File For Instructions On How To Enable And Disable It) */
void LCD_vInit_8BitMode(void);																							/* */
void LCD_vSendCmd_8BitMode(char Cmd_Needed);																			/* */
void LCD_vSendData_8BitMode(char data);																					/* */
void LCD_vPrintString_8BitMode(char *x);																				/* */
void LCD_Go_To_X_Y_8BitsMode(unsigned char row, unsigned char column);													/* */
																														/* */
/****************************************************** 4 Bits *************************************************************/
																														/* If The 4 Bits Mode Is Enabled The Following Functions Will Be Prototyped (Check The Macros File For Instructions On How To Enable And Disable It) */
void LCD_vInit_4BitMode(void);																							/* */
void LCD_vSendCmd_4BitMode(char Cmd_Needed);																			/* */
void LCD_vSendData_4BitMode(char data);																					/* */
void LCD_vPrintString_4BitMode(char *x);																				/* */
void LCD_Go_To_X_Y_4BitsMode(unsigned char row, unsigned char column);													/* */
																														/* */
/**************************************************** End Of Text **********************************************************/
#endif /********************************************************************************************************************/